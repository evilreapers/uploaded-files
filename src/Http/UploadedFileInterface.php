<?php

declare(strict_types=1);

namespace PSR\Http;

interface UploadedFileInterface
{
	/**
	 * Moves an uploaded file to the directory
	 * @param string $targetPath Directory to move the file
	 * @return bool
	 */
	public function moveTo(string $targetPath);

	/**
	 * Retrive the filename sent by the client
	 * @return string|null The filename sent by the client or null if none was provided
	 */
	public function getClientName();

	/**
	 * Retrive the file size
	 * @return int|null The file size in bytes or null if unknown.
	 */
	public function getSize();

	/**
	 * Retrive the error associated with uploaded file
	 * @return int One of PHP's UPLOAD_ERR_XXX constants.
	 */
	public function getError();


	/**
	 * Retrive the media type sent by the client
	 * @return string|null The media type sent by the client or null if none was provided
	 */
	public function getClientMediaType();

	/**
	 * Retrive the file extension sent by the client
	 * @return string|null The file extension sent by the client or null if none was provided
	 */
	public function getClientFileExt();
}
