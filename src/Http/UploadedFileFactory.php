<?php

declare(strict_types=1);

namespace PSR\Http;

class UploadedFileFactory
{
	/**
	 * Return UploadedFiles
	 * @param array $files $_FILES array
	 * @return \PSR\Http\UploadedFile[][]
	 */
	public static function createFromGlobal(array $files)
	{
		return UploadedFile::createFromGlobal($files);
	}
}
