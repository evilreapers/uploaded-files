<?php

declare(strict_types=1);

namespace PSR\Http;

class UploadedFile implements UploadedFileInterface
{
	private $name;
	private $type;
	private $tmpName;
	private $error = UPLOAD_ERR_OK;
	private $size;
	private $moved = false;

	public function __construct(array $file)
	{
		$this->name = $file['name'];
		$this->type = $file['type'];
		$this->tmpName = $file['tmp_name'];
		$this->error = $file['error'];
		$this->size = $file['size'];
	}

	public function moveTo(string $targetPath)
	{
		if($this->moved){
			throw new \RuntimeException('Uploaded file already moved');
		}

		if(!is_writable(dirname($targetPath))){
			throw new \InvalidArgumentException(sprintf('Upload target path %s is not writable', $targetPath));
		}
		
		if(!is_uploaded_file($this->tmpName)){
			throw new \RuntimeException(sprintf('%s is not a valid uploaded file', $this->tmpName));
		}

		if(!move_uploaded_file($this->tmpName, $targetPath)){
			throw new \RuntimeException(sprintf('Error moving uploaded file %s to %s', $this->name, $targetPath));
		}

		$this->moved = true;
	}

	public function getSize()
	{
		return $this->size;
	}

	public function getError()
	{
		return $this->error;
	}

	public function getClientName()
	{
		return $this->name;
	}

	public function getClientMediaType()
	{
		return $this->type;
	}

	public function getClientFileExt()
	{
		return pathinfo($this->name, PATHINFO_EXTENSION);
	}

	/**
	 * Creates a normalized tree of UploadedFile instances
	 * @param array $globals The global $_FILES variable
	 * @return \PSR\Http\UploadedFile[][] A normalized tree of UploadedFile instance
	 */
	public static function createFromGlobal(array $global): array
	{
		return static::parseUploadedFiles($global);
	}

	/**
	 * Parse a non-normalized, i.e. $_FILES superglobal, tree of uploaded file data.
	 *
	 * @internal This method is not part of the PSR-7 standard.
	 *
	 * @param array $uploadedFiles The non-normalized tree of uploaded file data.
	 *
	 * @return array A normalized tree of UploadedFile instances.
	 */
	private static function parseUploadedFiles(array $uploadedFiles): array
	{
		$parsed = [];
		foreach ($uploadedFiles as $field => $uploadedFile) {
			if (!isset($uploadedFile['error'])) {
				if (is_array($uploadedFile)) {
					$parsed[$field] = static::parseUploadedFiles($uploadedFile);
				}
				continue;
			}

			$parsed[$field] = [];
			if (!is_array($uploadedFile['error'])) {
				$parsed[$field] = new static([
					'tmp_name' => $uploadedFile['tmp_name'],
					'name' => isset($uploadedFile['name']) ? $uploadedFile['name'] : null,
					'type' => isset($uploadedFile['type']) ? $uploadedFile['type'] : null,
					'size' => isset($uploadedFile['size']) ? $uploadedFile['size'] : null,
					'error' => $uploadedFile['error'],
				]);
			} else {
				$subArray = [];
				foreach ($uploadedFile['error'] as $fileIdx => $error) {
					// Normalize sub array and re-parse to move the input's key name up a level
					$subArray[$fileIdx]['name'] = $uploadedFile['name'][$fileIdx];
					$subArray[$fileIdx]['type'] = $uploadedFile['type'][$fileIdx];
					$subArray[$fileIdx]['tmp_name'] = $uploadedFile['tmp_name'][$fileIdx];
					$subArray[$fileIdx]['error'] = $uploadedFile['error'][$fileIdx];
					$subArray[$fileIdx]['size'] = $uploadedFile['size'][$fileIdx];

					$parsed[$field] = static::parseUploadedFiles($subArray);
				}
			}
		}
		return $parsed;
	}
}
